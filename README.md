dig.js
======

| [dns-suite](https://git.coolaj86.com/coolaj86/dns-suite)
| **dig.js**
| [mdig.js](https://git.coolaj86.com/coolaj86/mdig.js)
| [digd.js](https://git.coolaj86.com/coolaj86/digd.js)
| Sponsored by [ppl](https://ppl.family)[.](https://dapliefounder.com)

Create and capture DNS and mDNS query and response packets to disk as binary and/or JSON.
Options are similar to the Unix `dig` command. Supports dns0x20 security checking.

Install
-------

### with git

```bash
# Install the latest of v1.x
npm install -g 'git+https://git.coolaj86.com/coolaj86/dig.js.git#v1'
```

```bash
# Install exactly v1.0.0
npm install -g 'git+https://git.coolaj86.com/coolaj86/dig.js.git#v1.0.0'
```

### without git

Don't have git? Well, you can also bow down to the gods of the centralized, monopolized, concentrated, *dictator*net
(as we like to call it here at ppl Labs), if that's how you roll:

```bash
npm install -g dig.js
```

Usage
-----

```bash
dig.js [TYPE] <domainname>
```

**Example**:

```bash
dig.js coolaj86.com
```

### mDNS Browser Example

This is pretty much an mDNS browser

```bash
dig.js --mdns _services._dns-sd._udp.local
```

Really the `--mdns` option is just an alias for setting all of these options as the default:

```bash
dig.js -p 5353 @224.0.0.251 PTR _services._dns-sd._udp.local +time=3
```

### Moar Examples

```bash
dig.js A coolaj86.com

dig.js @8.8.8.8 A coolaj86.com
```

Options
-------

```
--output <path/to/file>     write query and response(s) to disk with this path prefix (ex: ./samples/dns)

--mdns                      Use mDNS port and nameserver address, and listen for multiple packets

-t <type> (superfluous)     A, CNAME, MX, etc. Also supports -t type<decimal> for "unsupported" types. default ANY (mdns default: PTR)
-c <class>                  default IN
-p <port>                   default 53 (mdns default: 5353) (listener is random for DNS and 5353 for mDNS)
-q <query> (superfluous)    required (ex: coolaj86.com)
--nameserver <ns>           alias of @<nameserver>
--timeout <ms>              alias of +time=<seconds>, but in milliseconds

@<nameserver>               specify the nameserver to use for DNS resolution (defaults to system defaults)
+time=<seconds>             Sets the timeout for a query in seconds.
+norecurse                  Set `rd` flag to 0. Do not request recursion
+aaonly                     Set `aa` flag to 1.

--norecase         					Disable dns0x20 security checking (mixed casing). See https://dyn.com/blog/use-of-bit-0x20-in-dns-labels/
--recase           					Print the dns0x20 casing as-is rather than converting it back to lowercase. This is the default when explicitly using mixed case.

--debug                     verbose output
```

Security Concerns
-----------------

The 16-bit `id` of the query must match that of the response.

Extra entropy is added by using `dns0x20`, the de facto standard for RanDOmCASiNg on the query which must be matched in the response.
