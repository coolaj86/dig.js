'use strict';

var fs = require('fs');

module.exports = {
  types: [ 'A', 'AAAA', 'CNAME', 'MX', 'NS', 'PTR', 'SOA', 'SRV', 'TXT' ]
, printers: {
    'ANY': function (q) {
      console.log(';' + q.name + '.', q.ttl, (q.className || q.class), (q.typeName || ('type' + q.type)), q.data || q.rdata || 'unknown record type');
    }

  , 'A': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.address);
    }
  , 'AAAA': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.address);
    }
  , 'CNAME': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.data + '.');
    }
  , 'MX': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.priority + ' ' + q.exchange + '.');
    }
  , 'NS': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.data);
    }
  , 'PTR': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.data);
    }
  , 'SOA': function (q) {
      // no ';' in authority section?
      console.log('' + q.name + '.', q.ttl, q.className, q.typeName, q.name_server, q.email_addr, q.sn, q.ref, q.ret, q.ex, q.nx);
    }
  , 'SRV': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.priority + ' ' + q.weight + ' ' + q.port + ' ' + q.target);
    }
  , 'TXT': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, '"' + q.data.join('" "') + '"');
    }
  , 'CAA': function (q) {
      console.log(';' + q.name + '.', q.ttl, q.className, q.typeName, q.flag + ' ' + q.tag + ' "' + q.value + '"');
    }
  }
, writeQuery: function (opts, query, queryAb) {
    var path = require('path');
    var basename = query.question[0].name + '.'
      + (query.question[0].typeName||query.question[0].type.toString()).toLowerCase();
    var binname =  basename + '.query.bin';
    var jsonname = basename + '.query.json';
    var binpath = opts.output + '.' + binname;
    var jsonpath = opts.output + '.' + jsonname;
    var json = JSON.stringify(query, null, 2);
    if (-1 !== ['.', '/', '\\' ].indexOf(opts.output[opts.output.length -1])) {
      binpath = path.join(opts.output, binname);
      jsonpath = path.join(opts.output, jsonname);
    }

    fs.writeFile(binpath, Buffer.from(queryAb), null, function () {
      console.log('wrote ' + queryAb.byteLength + ' bytes to ' + binpath);
    });
    fs.writeFile(jsonpath, json, null, function () {
      console.log('wrote ' + json.length + ' bytes to ' + jsonpath);
    });
  }
, writeResponse: function (opts, query, nb, packet) {
    var me = this;
    me._count = me._count || 0;
    var path = require('path');
    var basename = query.question[0].name + '.'
      + (query.question[0].typeName||query.question[0].type.toString()).toLowerCase();
    var binname = basename + '.' + me._count + '.bin';
    var jsonname = basename + '.' + me._count + '.json';
    var binpath = opts.output + '.' + binname;
    var jsonpath = opts.output + '.' + jsonname;
    var json = JSON.stringify(packet, null, 2);
    if (-1 !== ['.', '/', '\\' ].indexOf(opts.output[opts.output.length -1])) {
      binpath = path.join(opts.output, binname);
      jsonpath = path.join(opts.output, jsonname);
    }

    me._count += 1;

    fs.writeFile(binpath, nb, null, function () {
      console.log('wrote ' + nb.byteLength + ' bytes to ' + binpath);
    });
    fs.writeFile(jsonpath, json, null, function () {
      console.log('wrote ' + json.length + ' bytes to ' + jsonpath);
    });
  }
};
